

public class Rental {
	private Movie movie;
	private int daysRented;
	
	public Rental(Movie amovie, int thedaysRented ) { 
		movie =amovie;
		daysRented= thedaysRented;
		
	}
	
	public Movie getMovie() {
		return movie;
	}

	public int getDaysRented() {
		
		return daysRented;
	}

}
